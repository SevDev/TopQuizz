package top.sevdev.topquizz;

import java.util.List;

/**
 * @author	SevDev on 15/12/2017
 */

public class Question {
	private String question;
	private List<String> listChoice;
	private int idxAnswer;

	public Question(String question, List<String> listChoice, int idxAnswer) {
		this.question = question;
		this.listChoice = listChoice;
		this.idxAnswer = idxAnswer;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public List<String> getListChoice() {
		return listChoice;
	}

	public void setListChoice(List<String> listChoice) {
		this.listChoice = listChoice;
	}

	public int getIdxAnswer() {
		return idxAnswer;
	}

	public void setIdxAnswer(int idxAnswer) {
		this.idxAnswer = idxAnswer;
	}
}
