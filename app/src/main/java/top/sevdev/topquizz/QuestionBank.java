package top.sevdev.topquizz;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author	SevDev on 15/12/2017
 */

public class QuestionBank {
	private List<Question> listQuestion;
	private int idxNextQuestion;

	public QuestionBank(List<Question> listQuestion) {
		this.listQuestion = listQuestion;

		Collections.shuffle(this.listQuestion);

		this.idxNextQuestion = 0;
	}

	public Question getQuestion() {
		if (idxNextQuestion == listQuestion.size()) {
			idxNextQuestion = 0;
		}
		return listQuestion.get(idxNextQuestion++);
	}

	public static QuestionBank generateQuestions() {
		Question q1 = new Question("What is the world's biggest island ?",
				Arrays.asList("Australia", "Greenland", "Iceland", null),
				1);

		Question q2 = new Question("What are the three primary colors ?",
				Arrays.asList("Red, Green and Blue", "Red, Yellow and Blue", null, null),
				1);

		Question q3 = new Question("How many carats are there in pure gold?",
				Arrays.asList("18", "20", "22", "24"),
				3);

		Question q4 = new Question("Which famous artist cut off part of his own ear?",
				Arrays.asList("Claude Monet", "Leonardo Da Vinci", "Pablo Picasso","Vincent Van Gogh"),
				3);

		Question q5 = new Question("Where was the first nuclear reactor built?",
				Arrays.asList("America", "China", "Europe", "Russia"),
				0);

		Question q6 = new Question("What is the name of the dog in the 'Back to the Future' films?",
				Arrays.asList("Einstein", "K-9", "Rex", "Morty"),
				0);

		Question q7 = new Question("What type of shark was Jaws?",
				Arrays.asList("Bull Shark", "Great White Shark", "Hammerhead Shark", null),
				1);

		Question q8 = new Question("Which solar system planet experiences the hottest surface temperature?",
				Arrays.asList("Jupiter", "Mercury", "Sun", "Venus"),
				3);

		Question q9 = new Question("Where are the headquarters of UNO situated?",
				Arrays.asList("Geneva", "Hague (Netherlands)", "New York", "Paris"),
				2);

		Question q10 = new Question("Where is Mount Everest located in?",
				Arrays.asList("China", "India", "Nepal", "Tibet"),
				2);

		return new QuestionBank(Arrays.asList(q1, q2, q3, q4, q5, q6, q7, q8, q9, q10));
	}
}
