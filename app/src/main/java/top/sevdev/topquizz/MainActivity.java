package top.sevdev.topquizz;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author	SevDev on 15/12/2017
 */

public class MainActivity extends AppCompatActivity {
	private TextView txtWelcome;
	private EditText inpName;
	private Button btnPlay;

	public static final int GAME_ACTIVITY_REQUEST_CODE = 7;

	private SharedPreferences preferences;

	public static final String PREF_KEY_USERNAME = "PREF_KEY_USERNAME";
	public static final String PREF_KEY_SCORE = "PREF_KEY_SCORE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		preferences = getPreferences(MODE_PRIVATE);

		txtWelcome = findViewById(R.id.activity_main_txtWelcome);
		inpName = findViewById(R.id.activity_main_inpName);
		btnPlay = findViewById(R.id.activity_main_btnPlay);


		inpName.setHint(getResources().getString(R.string.hint_name));
		btnPlay.setText(getResources().getString(R.string.play));
		btnPlay.setEnabled(true);

		welcomeUser();

		inpName.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
				if (preferences.getString(PREF_KEY_USERNAME, null) == null) {
					btnPlay.setEnabled(charSequence.length() > 0);
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});

		btnPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent gameActivity = new Intent(MainActivity.this, GameActivity.class);
				startActivityForResult(gameActivity, GAME_ACTIVITY_REQUEST_CODE);
				User user = new User();
				user.setUsername(inpName.getText().toString());
				preferences.edit().putString(PREF_KEY_USERNAME, user.getUsername()).apply();
			}
		});
	}

	private void welcomeUser() {
		String username = preferences.getString(PREF_KEY_USERNAME, null);
		String score = preferences.getString(PREF_KEY_SCORE, null);
		String strWelcome;
		System.out.println(username);
		System.out.println(score);
		System.out.println(username == null);
		if (username == null) {
			System.out.println("null");
			inpName.setVisibility(View.VISIBLE);
			btnPlay.setEnabled(false);
			strWelcome = getResources().getString(R.string.welcome_first_launch);
		} else {
			System.out.println("notnull");
			inpName.setVisibility(View.INVISIBLE);
			btnPlay.setEnabled(true);
			if (score == null) {
				strWelcome = getResources().getString(R.string.welcome_user)
						+ " " + username + ", "
						+ getResources().getString(R.string.score_not_played);
			} else {
				strWelcome = getResources().getString(R.string.welcome_back_user)
						+ " " + username + ", "
						+ getResources().getString(R.string.last_score)
						+ " " + score;
			}
		}

		txtWelcome.setText(strWelcome);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_settings:
				final EditText inpName = new EditText(this);
				inpName.setText(preferences.getString(PREF_KEY_USERNAME, null));

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getResources().getString(R.string.settings))
						.setMessage(getResources().getString(R.string.username) + ":")
						.setView(inpName)
						.setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								preferences.edit().putString(PREF_KEY_USERNAME, inpName.getText().toString()).apply();
								recreate();
							}
						})
						.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
							}
						})
						.create()
						.show();
				return true;

			default:
				// If we got here, the user's action was not recognized.
				// Invoke the superclass to handle it.
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (GAME_ACTIVITY_REQUEST_CODE == requestCode && RESULT_OK == resultCode) {
			String score = data.getStringExtra(GameActivity.BUNDLE_EXTRA_SCORE);
			preferences.edit().putString(PREF_KEY_SCORE, score).apply();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		welcomeUser();
	}
}
