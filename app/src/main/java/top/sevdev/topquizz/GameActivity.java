package top.sevdev.topquizz;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author	SevDev on 15/12/2017
 */

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
	private TextView txtQuestion;
	private Button[] btnsAnswer = new Button[4];

	private QuestionBank questionBank;
	private Question crntQuestion;

	private int numQuestions;
	private int numQuestionsRemaining;

	private int score;

	public static final String BUNDLE_EXTRA_SCORE = "BUNDLE_EXTRA_SCORE";
	public static final String BUNDLE_STATE_SCORE = "currentScore";
	public static final String BUNDLE_STATE_QUESTION = "currentQuestion";

	private boolean enableTouchEvents;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_game);

		numQuestions = 5;

		if (savedInstanceState != null) {
			numQuestionsRemaining = savedInstanceState.getInt(BUNDLE_STATE_QUESTION);
			score = savedInstanceState.getInt(BUNDLE_STATE_SCORE);
		} else {
			score = 0;
			numQuestionsRemaining = numQuestions;
		}

		enableTouchEvents = true;

		txtQuestion = findViewById(R.id.activity_game_txtQuestion);
		btnsAnswer[0] = findViewById(R.id.activity_game_btnAnswer1);
		btnsAnswer[1] = findViewById(R.id.activity_game_btnAnswer2);
		btnsAnswer[2] = findViewById(R.id.activity_game_btnAnswer3);
		btnsAnswer[3] = findViewById(R.id.activity_game_btnAnswer4);

		for (int i = 0; i < btnsAnswer.length; i++) {
			btnsAnswer[i].setTag(i);
		}

		for (Button btn : btnsAnswer) {
			btn.setOnClickListener(this);
		}

		questionBank = questionBank.generateQuestions();
		crntQuestion = questionBank.getQuestion();
		this.displayQuestion(crntQuestion);
	}

	public void onClick(View v) {
		int idxResponse = (int) v.getTag();
		enableTouchEvents = false;

		if (crntQuestion.getIdxAnswer() == idxResponse) {
			Toast.makeText(this, getResources().getString(R.string.answerGood), Toast.LENGTH_SHORT).show();
			score++;
		} else {
			Toast.makeText(this, getResources().getString(R.string.answerBad), Toast.LENGTH_SHORT).show();
		}
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				enableTouchEvents = true;

				if (--numQuestionsRemaining == 0) {
					finishGame();
				} else {
					crntQuestion = questionBank.getQuestion();
					displayQuestion(crntQuestion);
				}
			}
		}, 2000);
	}

	private void displayQuestion(final Question question) {
		String strQuestion = getResources().getString(R.string.question)
				+ " " + (numQuestions + 1 - numQuestionsRemaining)
				+ "/" + numQuestions + " :\n" + question.getQuestion();
		txtQuestion.setText(strQuestion);
		for (int i = 0; i < btnsAnswer.length; i++) {
			if (question.getListChoice().get(i) == null) {
				btnsAnswer[i].setVisibility(View.INVISIBLE);
				btnsAnswer[i].setText("");
			} else {
				btnsAnswer[i].setVisibility(View.VISIBLE);
				btnsAnswer[i].setText(question.getListChoice().get(i));
			}
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return enableTouchEvents && super.dispatchTouchEvent(ev);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(BUNDLE_STATE_QUESTION, numQuestionsRemaining);
		outState.putInt(BUNDLE_STATE_SCORE, score);

		super.onSaveInstanceState(outState);
	}

	private void finishGame() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(getResources().getString(R.string.well_done))
				.setMessage(getResources().getString(R.string.announce_score) + " " + score + "/" + numQuestions)
				.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent = new Intent();
						intent.putExtra(BUNDLE_EXTRA_SCORE, score + "/" + numQuestions);
						setResult(RESULT_OK, intent);
						finish();
					}
				})
				.create()
				.show();
	}
}
