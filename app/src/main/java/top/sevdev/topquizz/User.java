package top.sevdev.topquizz;

/**
 * @author	SevDev on 15/12/2017
 */

public class User {
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
